FROM python:3.9

LABEL maintainer="Mark Crewson <mark@crewson.net>"

RUN pip install --no-cache-dir "uvicorn[standard]" gunicorn

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY ./gunicorn_conf.py /gunicorn_conf.py

COPY ./app /app
WORKDIR /app/

ENV PYTHONPATH=/app

EXPOSE 80

# Run the entrypoint script, it will check for an /app/prestart.sh script (e.g. for migrations)
# And then will start Gunicorn with Uvicorn
ENTRYPOINT ["/entrypoint.sh"]
